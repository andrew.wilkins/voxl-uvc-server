# voxl-uvc-server

Application to send video frames from UVC cameras to pipes (Modal Pipe Architecture).

Supports UVC cameras that have raw frame output. Tested against:
* Logitech C270
* Videology 5MP Micro USB 2.0 Board Camera
* PureThermal Mini Pro with Flir Lepton
* Flir Boson 640

dependencies:
* libmodal_pipe
* voxl-uvc-server

This README covers building this package. The voxl-uvc-server user manual is located [here](https://docs.modalai.com/voxl-uvc-server/)


## Build Instructions

#### Prerequisites

* voxl-emulator for voxl builds: https://gitlab.com/voxl-public/voxl-docker
* qrb5165-emulator for qrb5165 builds: https://gitlab.com/voxl-public/utilities/voxl-docker/-/tree/dev

#### Clean out all old artifacts

```bash
$ ./clean.sh
```

#### Build for Voxl

1) Launch Docker

```bash
~/git/voxl-uvc-server$ voxl-docker -i voxl-emulator
voxl-emulator:~$
```

2) Install build dependencies from desired branch

```
voxl-emulator:~$ ./install_build_deps.sh <dev | stable>
```

3) Compile inside the docker.

```bash
voxl-emulator:~$ ./build.sh
```

4) Make an ipk package inside the docker.

```bash
voxl-emulator:~$ ./make_package.sh
Making Normal Package

Package Name:  voxl-uvc-server
version Number:  0.0.8
Creating an ipk for voxl
starting building voxl-uvc-server_0.0.8.ipk
/usr/bin/ar: creating voxl-uvc-server_0.0.8.ipk

DONE
```

This will make a new voxl-uvc-server_x.x.x.ipk file in your working directory. The name and version number came from the pkg/control/control file. If you are updating the package version, edit it there.

#### Build for QRB5165

1) Launch Docker

```bash
~/git/voxl-uvc-server$ voxl-docker -i qrb5165-emulator
sh-4.4#
```

2) Install build dependencies from desired branch

```
sh-4.4# ./install_build_deps.sh <dev | stable>
```

3) Compile inside the docker.

```bash
sh-4.4# ./build.sh
```

4) Make a deb package inside the docker.

```bash
sh-4.4# ./make_package.sh
Making Normal Package

Package Name:  voxl-uvc-server
version Number:  0.0.8
Creating a debian package for qrb5165
starting building Debian Package
dpkg-deb: building package 'voxl-uvc-server' in 'voxl-uvc-server_0.0.8.deb'.

DONE  
```

This will make a new voxl-uvc-server_x.x.x.deb file in your working directory. The name and version number came from the pkg/control/control file. If you are updating the package version, edit it there.


## Deploy to VOXL / QRB5165

You can now push the package to the board and install with the package manager however you like.
To do this over ADB, you may use the included helper script: deploy_to_voxl.sh.

Do this OUTSIDE of docker as your docker image probably doesn't have usb permissions for ADB.

```bash
$ ./deploy_to_voxl.sh
```
