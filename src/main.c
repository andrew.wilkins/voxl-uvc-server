/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <modal_pipe_server.h>

#include "libuvc/libuvc.h"

#define SERVER_NAME   "voxl-uvc-server"
#define UVC_PIPE_NAME "uvc"
#define UVC_PIPE_TYPE "camera_image_metadata_t"

#define NUM_SUPPORTED_FRAME_FORMATS 3

static bool server_running = true;
static bool server_debug = false;
static bool mpa_debug = false;
static bool first_frame_received = false;

static enum uvc_frame_format supported_frame_format[NUM_SUPPORTED_FRAME_FORMATS] =
     {UVC_FRAME_FORMAT_YUYV, UVC_FRAME_FORMAT_UYVY, UVC_FRAME_FORMAT_NV12};
static int supported_mpa_frame_format[NUM_SUPPORTED_FRAME_FORMATS] =
     {IMAGE_FORMAT_YUV422, IMAGE_FORMAT_YUV422_UYVY, IMAGE_FORMAT_NV12};
static int mpa_frame_format = -1;

// Used to capture ctrl-c signal to allow graceful exit
void intHandler(int dummy) {
    if (server_debug) printf("Got SIGINT, exiting\n");
    server_running = false;
}

// This callback function runs once per frame. Use it to perform any
// quick processing you need, or have it put the frame into your application's
// input queue. If this function takes too long, you'll start losing frames.
void cb(uvc_frame_t *frame, void *ptr) {

    if ( ! first_frame_received) {
        if (server_debug) {
            printf("Got frame callback! frame_format = %d, width = %d, height = %d, length = %lu, ptr = %p\n",
                   frame->frame_format, frame->width, frame->height, frame->data_bytes, ptr);
        }

        first_frame_received = true;
    }

    // Prepare meta data for this frame
    camera_image_metadata_t meta = { \
        .magic_number = CAMERA_MAGIC_NUMBER, \
        .frame_id = frame->sequence, \
        .width = frame->width, \
        .height = frame->height, \
        .size_bytes = frame->data_bytes, \
        .stride = frame->width * 2, \
        .exposure_ns = 100, \
        .gain = 100, \
        .format = mpa_frame_format, \
        .reserved = 0 };

    // Apply proper timestamp
    struct timespec time;
    (void) clock_gettime(CLOCK_MONOTONIC, &time);
    meta.timestamp_ns = ((uint64_t) time.tv_sec) * 1000000000;
    meta.timestamp_ns += time.tv_nsec;

    // Submit metadata and frame to the pipe
    int status = pipe_server_write_camera_frame(0, meta, (const void*) frame->data);

    if (server_debug && (frame->sequence % 30 == 0)) {
        printf(" * got image %u\n",  frame->sequence);
    }
}

void help() {
    printf("Usage: voxl-uvc-server <options>\n");
    printf("Options:\n");
    printf("-d                Show extra debug messages\n");
    printf("-m                Show extra MPA debug messages\n");
    printf("-v <vendor-id>    USB vendor id of the desired UVC device in hexadecimal (e.g. 090c)\n");
    printf("                  Default is search for any vendor id\n");
    printf("-p <product-id>   USB product id of the desired UVC device in hexadecimal (e.g. 337b)\n");
    printf("                  Default is search for any product id\n");
    printf("-r <resolution>   Desired image resolution (e.g. 320x240)\n");
    printf("                  Default is 640x480\n");
    printf("-f <fps>          Desired frame rate in fps (e.g. 15)\n");
    printf("                  Default is 30\n");
    printf("-t <pipe name>    Custom mpa name\n");
    printf("                  Default is uvc\n");
    printf("-s                Show information about all devices found then exit\n");
    printf("-h                Show help\n");
}

int main(int argc, char *argv[]) {
    int return_status = 0;
    bool list_devices = false;
    int vendor_id = 0;
    int product_id = 0;
    int width = 640;
    int height = 480;
    int fps = 30;
    char pipe_name[100];

    // Parse all command line options
    int opt = 0;
    while ((opt = getopt(argc, argv, "sdmv:p:r:f:t:h")) != -1) {
        switch (opt) {
        case 's':
            list_devices = true;
            break;
        case 'd':
            printf("Enabling debug messages\n");
            server_debug = true;
            break;
        case 'm':
            printf("Enabling MPA debug messages\n");
            mpa_debug = true;
            break;
        case 'v':
            vendor_id = (int) strtol(optarg, NULL, 16);
            break;
        case 'p':
            product_id = (int) strtol(optarg, NULL, 16);
            break;
        case 'r':
        {
            char* w = strtok(optarg, "x");
            char* h = strtok(NULL, "x");
            if(w == NULL || h == NULL) {
                fprintf(stderr, "Error - incorrect resolution format: WIDTHxHEIGHT\n\n");
                return -1;
            } else {
                width = atoi(w);
                height = atoi(h);
            }
            break;
        }
        case 'f':
            fps = (int) strtol(optarg, NULL, 10);
            break;
        case 't':
            strcpy(pipe_name,optarg);
            printf("pipe_name:%s\n",pipe_name);
            break;
        case 'h':
            help();
            return -1;
        case '?':
            fprintf(stderr, "Error - unknown option: %c\n\n", optopt);
            help();
            return -1;
        }
    }

    if (server_debug) printf("voxl-uvc-server starting\n");
    if ((server_debug) && (vendor_id)) printf("Vendor ID 0x%.4x chosen\n", vendor_id);
    if ((server_debug) && (product_id)) printf("Product ID 0x%.4x chosen\n", product_id);
    if (server_debug) printf("Image resolution %dx%d, %d fps chosen\n", width, height, fps);

    // optind is for the extra arguments which are not parsed
    for (; optind < argc; optind++) {
        fprintf(stderr, "extra arguments: %s\n", argv[optind]);
        help();
        return -1;
    }

    // Setup our signal handler to catch ctrl-c
    signal(SIGINT, intHandler);

    uvc_context_t *ctx;
    uvc_device_t *dev;
    uvc_device_handle_t *devh;
    uvc_stream_ctrl_t ctrl;
    uvc_error_t res;

    // Initialize a UVC service context. Libuvc will set up its own libusb
    // context. Replace NULL with a libusb_context pointer to run libuvc
    // from an existing libusb context.
    res = uvc_init(&ctx, NULL);

    if (res < 0) {
        fprintf(stderr, "Error: uvc_init failed\n");
        return res;
    }

    if (server_debug) printf("UVC initialized\n");

    // If the list devices option was provided on the command line we will cycle
    // through all of the devices we find and print out a bunch of diagnostic
    // information. If that fails for any of the devices in the list the program
    // will return an error code. The program exists after this operation.
    if (list_devices) {
        uvc_device_t **dev_list;
        uint32_t num_devices = 0;

        printf("*** START DEVICE LIST ***\n");

        res = uvc_get_device_list(ctx, &dev_list);
        if (res == UVC_SUCCESS) {
            uint32_t i = 0;
            uvc_device_descriptor_t *desc = NULL;

            while (dev_list[i] != NULL) {
                num_devices++;
                printf("\nFound device %u\n\n", num_devices);

                res = uvc_get_device_descriptor(dev_list[i], &desc);
                if (res == UVC_SUCCESS) {
                    printf("Got device descriptor for %.4x:%.4x %s\n\n", desc->idVendor, desc->idProduct, desc->serialNumber);

                    res = uvc_find_device(ctx, &dev, desc->idVendor, desc->idProduct, desc->serialNumber);
                    if (res == UVC_SUCCESS) {
                        printf("Found device %.4x:%.4x\n\n", desc->idVendor, desc->idProduct);

                        res = uvc_open(dev, &devh);
                        if (res == UVC_SUCCESS) {
                            // Print out a message containing all the information that libuvc
                            // knows about the device
                            uvc_print_diag(devh, stdout);
                            uvc_close(devh);
                        } else {
                            fprintf(stderr, "uvc_open failed, Return code: ");
                            uvc_perror(res, NULL);
                            return_status = -1;
                        }
                    } else {
                        fprintf(stderr, "uvc_find_device failed, Return code: ");
                        uvc_perror(res, NULL);
                        return_status = -1;
                    }
                    uvc_free_device_descriptor(desc);
                    desc = NULL;
                } else {
                    fprintf(stderr, "uvc_get_device_descriptor failed, Return code: ");
                    uvc_perror(res, NULL);
                    return_status = -1;
                }
                i++;
            }
            uvc_free_device_list(dev_list, num_devices);
        }

        if (num_devices == 0) {
            printf("\nNo devices found\n");
        }

        printf("\n*** END DEVICE LIST ***\n");
        goto exit;
    }

    // Locates the first attached UVC device, stores in dev.
    // You can filter devices by vendor_id, product_id, "serial_num"
    res = uvc_find_device(ctx, &dev, vendor_id, product_id, NULL);

    // no devices found
    if (res < 0) {
        if ((vendor_id) || (product_id)) fprintf(stderr, "uvc_find_device failed for %.4x:%.4x\n", vendor_id, product_id);
        else fprintf(stderr, "uvc_find_device failed\n");
        return_status = -1;
        goto exit;
    }

    if (server_debug) {
        if ((vendor_id) || (product_id)) printf("Device %.4x:%.4x found\n", vendor_id, product_id);
        else printf("Device found\n");
    }

    // Try to open the device: requires exclusive access
    res = uvc_open(dev, &devh);

    if (res < 0) {
        fprintf(stderr, "uvc_open failed\n");
        return_status = -1;
        goto exit;
    }

    if (server_debug) printf("Device opened\n");

    // Negotiate desired stream configuration
    int i = 0;
    for (; i < NUM_SUPPORTED_FRAME_FORMATS; i++) {
        res = uvc_get_stream_ctrl_format_size(
            devh, &ctrl, // result stored in ctrl
            supported_frame_format[i],
            width, height, fps
        );
        mpa_frame_format = supported_mpa_frame_format[i];
        if (res == UVC_SUCCESS) break;
    }

    if (res < 0) {
        fprintf(stderr, "uvc_get_stream_ctrl_format_size failed\n");
    } else if (server_debug) printf("uvc_get_stream_ctrl_format_size succeeded for format %d\n", i);

    // Print out the result
    // if (server_debug) uvc_print_stream_ctrl(&ctrl, stdout);

    // create the pipe for MPA
    pipe_info_t info = { \
        .name        = UVC_PIPE_NAME,\
        .location    = UVC_PIPE_NAME,\
        .type        = UVC_PIPE_TYPE,\
        .server_name = SERVER_NAME,\
        .size_bytes  = (256 * MODAL_PIPE_DEFAULT_PIPE_SIZE) };

    //custom pipe
    if(strlen(pipe_name) > 0){
        strcpy(info.name,pipe_name);
        strcpy(info.location,pipe_name);
    }

    int mpa_flags = 0;
    if (mpa_debug) mpa_flags = SERVER_FLAG_EN_DEBUG_PRINTS;

    int status = pipe_server_create(0, info, mpa_flags);

    if (status) {
        fprintf(stderr, "pipe_server_create failed\n");
        return -1;
    }

    // Start the video frame streaming
    res = uvc_start_streaming(devh, &ctrl, cb, NULL, 0);

    if (res < 0) {
        fprintf(stderr, "uvc_start_streaming failed\n");
        return res;
    } else if (server_debug) printf("Streaming starting\n");

    // Just sit in an endless loop until ctrl-c is pressed or we time out on
    // receiving any frames
    int frame_timeout = 0;
    while (server_running) {
        sleep(1);
        if (first_frame_received == false) {
            if (frame_timeout++ == 5) {
                break;
            }
        }
    }

    // End the stream. Blocks until last callback is serviced
    uvc_stop_streaming(devh);

    pipe_server_close_all();

    if (server_debug) printf("Done streaming\n");

    if (res < 0) {
        fprintf(stderr, "uvc_start_streaming failed\n");
        return res;
    }

    uvc_close(devh);

exit:
    // Close the UVC context. This closes and cleans up any existing device handles,
    // and it closes the libusb context if one was not provided.
    uvc_exit(ctx);

    if (server_debug) printf("UVC exited\n");

    if (server_debug) printf("voxl-uvc-server ending\n");

    return return_status;
}
