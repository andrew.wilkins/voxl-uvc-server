cmake_minimum_required(VERSION 3.3)
project(voxl-uvc-server)

message("BUILDSIZE=${BUILDSIZE}")

add_definitions(-D_GNU_SOURCE)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O0 -g -std=c11 -Wall -pedantic")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -D_XOPEN_SOURCE=500 -fno-strict-aliasing")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fdata-sections -fno-zero-initialized-in-bss")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -Wno-unused-parameter")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-unused-function -Wno-unused-variable")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-cast-align")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-missing-braces -Wno-strict-aliasing")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -D__FILENAME__='\"$(subst ${CMAKE_SOURCE_DIR}/,,$(abspath $<))\"'")

include_directories( include/ )

if(${BUILDSIZE} MATCHES 64)
    message("BUILDSIZE matches 64")
    find_library(MODAL_JSON modal_json HINTS /usr/lib64)
    find_library(MODAL_PIPE modal_pipe HINTS /usr/lib64)
else()
    message("BUILDSIZE does not match 64")
    find_library(MODAL_JSON modal_json HINTS /usr/lib)
    find_library(MODAL_PIPE modal_pipe HINTS /usr/lib)
endif()

add_executable( voxl-uvc-server
                src/main.c)

target_link_libraries(voxl-uvc-server
                      ${MODAL_JSON}
                      ${MODAL_PIPE}
                      uvc)

install(
    TARGETS voxl-uvc-server
    DESTINATION /usr/bin
)
